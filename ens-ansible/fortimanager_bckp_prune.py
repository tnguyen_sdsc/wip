#!/bin/python3
#BY: Thanh Nguyen
#VERSION: 0.1
#DATE: 20221012
#WHAT: This script will prune file older than a certain number of days

import os, time, sys, fnmatch

# These are variables that can be changed
days = 7 # number of days to keep from now (Not number of files to keep)
path = r"/opt/fortimanager-backups/backups" # Path of files to prune
fmatch = "fortimanager"

# Should not change anything below here unless changing the way this script works
now = time.time() # assign now current time
ffmatch = "*"+fmatch+"*"

for f in os.listdir(path):
  if fnmatch.fnmatch(f,ffmatch):
    g = os.path.join(path, f) # join path and file
    if os.stat(g).st_mtime < now - days * 86400:  # if file is less than x days old. x=days variable
      if os.path.isfile(g): # check if file exists
        print("Removing",g)  # print to screen what files are being deleted
        os.remove(g) # remove the file that meet condition of x days old and exists
