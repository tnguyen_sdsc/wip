#!/usr/bin/python3
#BY: Thanh Nguyen
#VERSION: 0.1
#DATE: 20221001
#WHAT:  example: deluser.py passwd1 user1
#       takes is file passwd1 and search for string user1 and delete that line


import sys
import os.path
import re
import subprocess


# Check for 2 arguments. 
# Return True if have 2 arguments, 
# else return False
def check_args():
    if len(sys.argv) == 3:
        global passwdFile   # make passwdFile a global variable for use in main()
        passwdFile = sys.argv[1]
        global user2delete  # make user2delete a global variable for use in main()
        user2delete = sys.argv[2]
        return True
    else:
        return False

# Check to see if "file" exists
# return True if exist, else False
def check_file_exist(file):
    if True == os.path.exists(file):
        return True
    else:
        return False

# Check to see if string "user" exists in file "pwfile"
def check_user(pwfile, user):
    out = None # python for null - default result is string "not found" or None
    with open(pwfile) as file:  # open file for reading
        for line in file:       # read each line from file
            if re.search(r'\b' + line.split(":")[0] + r'\b', user): # search for "user" with deliminator ":"
                out = "Found" # set out to "Found"
                return True
        if out is None:     # if out is Null return false
#            print(user,out,"in", pwfile,"return False")
            return False

# This functions delete line with string "user" from file "pwfile"
def remove_user(pwfile, user):
     with open(pwfile) as file:
        for line in file:
            if line.split(":")[0] != user:  # search for string "user" in first column delimited by ":"
                f = open(r"passwd_tmp","a") # if string is NOT found in line, write to temp file
                f.write(line)               # don't write line if string IS found
        os.replace('passwd_tmp',pwfile)     # replace pwfile with temp file, resulting in file w/o line containing string

def checkout(file):                         # Uses RCS to check out file
    subprocess.run(["co","-l",file])

def checkin(user2delete, file):             # Uses RCS to check in file, message will be user deleted
    message = "deleted", user2delete        # user2delete is global variable from function check_args()
    msg_arg = "-m"+" ".join(message)       # passing -mMessage can't have space with ci -u -mMessage
    subprocess.run(["ci","-u",msg_arg,file])    # command: ci -u -mMessage file


# This is the main function.  check_args() has to be called for all other functions to work.
# It set the global variables passwdFile and user2delete.
def main():
    if not check_args():                    # If doesn't return w/ 2 arguments, stop and show Usage
        print ("Usage: clean_pass.py [passwd_file] [username]")
    elif not check_file_exist(passwdFile): 
        print("passwd file",passwdFile," doesn't exist, please check.")
    elif not check_user(passwdFile, user2delete):
        print("User",user2delete,"does not exists in",passwdFile)
    else:
        print("checking out file", passwdFile)
        checkout(passwdFile)
        print("Removing", user2delete, "from", passwdFile)
        remove_user(passwdFile, user2delete)
        print("checking in file", passwdFile)
        checkin(user2delete, passwdFile)

if __name__ == "__main__":
    main()

